echo "Status"
drush ms
echo "Reset Migrations"
drush mrs td_audios
drush mrs td_thumbnails
drush mrs td_videos
drush mrs td_hosts
drush mrs td_hosts_2
drush mrs td_episodes
echo "Reinstalling module"
drush pmu talkingdrupal_migration
drush en talkingdrupal_migration
echo "Running td_audios migration"
drush mim td_audios
echo "Running td_thumbnails migration"
drush mim td_thumbnails
echo "Running td_videos migration"
drush mim td_videos
echo "Running td_hosts migration"
drush mim td_hosts
echo "Running td_hosts_2 migration"
drush mim td_hosts_2
echo "Running td_episodes migration"
drush mim td_episodes
echo "Status"
drush ms