<?php

namespace Drupal\td_utilities\Drush\Commands;

use Consolidation\OutputFormatters\StructuredData\RowsOfFields;
use Drupal\Core\Entity\EntityTypeManagerInterface;
use Drupal\Core\Utility\Token;
use Drupal\media\Entity\Media;
use Drush\Attributes as CLI;
use Drush\Commands\DrushCommands;
use Symfony\Component\DependencyInjection\ContainerInterface;

/**
 * A Drush commandfile.
 *
 * In addition to this file, you need a drush.services.yml
 * in root of your module, and a composer.json file that provides the name
 * of the services file to use.
 */
final class TdUtilitiesCommands extends DrushCommands {

  /**
   * Constructs a TdUtilitiesCommands object.
   */
  public function __construct(
    private readonly Token $token,
    private readonly EntityTypeManagerInterface $entityTypeManager,
  ) {
    parent::__construct();
  }

  /**
   * {@inheritdoc}
   */
  public static function create(ContainerInterface $container) {
    return new static(
      $container->get('token'),
      $container->get('entity_type.manager'),
    );
  }

  /**
   * Command description here.
   */
  #[CLI\Command(name: 'td_yv:move', aliases: ['tdyv'])]
  #[CLI\FieldLabels(labels: [
    'nid' => 'Id',
    'youtubeURL' => 'youtubeURL',
  ])]
  #[CLI\Usage(name: 'td_yv:names tdyv', description: 'Run once')]
  public function videoMigrate() {
    $rows = [];
    $query = $this->entityTypeManager->getStorage('node')->getQuery();
    $query->condition('type', 'podcast_episode');
    $pageIds = $query->accessCheck(FALSE)->execute();

    foreach ($pageIds as $id) {
      $videoUrl = 'none';
      $node = $this->entityTypeManager->getStorage('node')->load($id);
      $mediaId = $node->schema_video->target_id;
      if ($mediaId) {
        $video = $node->get('schema_video')->entity;
        if ($video instanceof Media) {
          $videoUrl = $video->field_media_oembed_video->value;
          $node->set('field_video', $videoUrl);
          $node->save();
        }
      }

      $rows[] = [
        'nid' => $id,
        'youtubeURL' => $videoUrl,
      ];
    }

    return new RowsOfFields($rows);
  }
}
