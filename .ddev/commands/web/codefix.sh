#!/bin/bash

## Description: Sniff code
## Usage: cdf
## Example: cdf

./vendor/bin/phpcbf \
  --standard="Drupal,DrupalPractice" -n \
  --extensions="php,module,inc,install,test,profile,theme" \
  --ignore="web/themes/custom/td/node_modules" \
  web/themes/custom/td \
  web/modules/custom