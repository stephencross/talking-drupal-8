#!/bin/bash
## Description: Sync local
## Usage: refresh
## Example: "ddev refresh"
## ProjectTypes: drupal10

composer install
read -p "Would you like to replace the database? [Y/n] (yes): " answer
if [ -z "$answer" ]; then
    answer="Y"
fi

case "$answer" in
    [Yy] | [Yy][Ee][Ss])
        platform db:dump -p spzxjmhonxagm -e main -y
        drush sql-drop -y
        drush sql-cli < /var/www/html/spzxjmhonxagm--main-bvxea6i--db--main--dump.sql
        ;;
    *)
        echo "The DB has not been changed."
        ;;
esac
drush updb -y
drush cr
drush cim -y
drush uli
platform mount:download -p spzxjmhonxagm -e main --mount web/sites/default/files --target /var/www/html/web/sites/default/files -y
